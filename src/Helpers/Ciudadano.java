/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Helpers;

/**
 *
 * @author Gladys Escaño
 */
public class Ciudadano {
     //Mi nombre
    private String nombre;
    
    //Mi edad
    private int edad;
    
    //Años de experiencia
    private int anoexpe;
    
    public void setnombre(String _nombre){
        this.nombre = _nombre;
    }
    
    //Métodos Set y Get nombre
    public String getnombre(){
        return nombre;
    }
    
    //Métodos Set y Get Edad
    public void setedad(int _edad){
        this.edad = _edad;
    }
    
    public int getedad(){
        return edad;
    }
    
    //Métodos Set y Get Años de experiencia
    public void setanoexperiencia(int _anoexpe){
        this.anoexpe = _anoexpe;
    }   
    public int getanoexperiencia(){
        return anoexpe;
    }      
}
