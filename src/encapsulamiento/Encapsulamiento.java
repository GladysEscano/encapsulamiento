/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encapsulamiento;

import Helpers.Ciudadano;

/**
 *
 * @author Gladys Escaño
 */
public class Encapsulamiento {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Ciudadano ciudadano = new Ciudadano();
        
        //Establecer valor a variable privada
        ciudadano.setnombre("Gladys Escaño");
        ciudadano.setedad(23);
        ciudadano.setanoexperiencia(4);
        
        //Imprimir valor de variables privadas
        System.out.println(ciudadano.getnombre());
        System.out.println(ciudadano.getedad());
        System.out.println(ciudadano.getanoexperiencia());
        
    }
    
}
